<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/package/{id?}', 'PackageController@index')->name('package.index');
Route::post('/package', 'PackageController@store')->name('package.store');
Route::put('/package/{id}', 'PackageController@update')->name('package.update');
Route::patch('/package/{id}', 'PackageController@update')->name('package.update');
Route::delete('/package/{id}', 'PackageController@destroy')->name('package.delete');

Route::get('/testing', 'TestingController@index');
