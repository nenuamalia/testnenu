<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use \App\Package;
use \App\User;

class PackageControllerTest extends TestCase
{
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function teststorePackage()
    {
        $user    = factory(User::class)->create();
        $package = factory(Package::class)->create();

        $response = $this->actingAs($package)
            ->json('POST', '/package', [
                'transaction_id'            => $this->faker->unique(),
                'customer_name'             => $this->faker->word(2, true),
                'customer_code'             => $this->faker->randomNumber(7),
                'transaction_amount'        => $this->faker->randomNumber(7),
                'transaction_discount'      => $this->faker->randomNumber(2),
                'transaction_payment_type'  => $this->faker->randomNumber(2),
                'transaction_state'         => $this->faker->word(2, true),
                'transaction_code'          => $this->faker->word(2, true),
                'transaction_order'         => $this->faker->randomNumber(2),
                'location_id'               => $this->faker->word(2, true),
                'organization_id'           => $this->faker->randomNumber(1),
                'transaction_payment_type_name' => $this->faker->word(2, true),
                'customer_attribute'        => $this->faker->word(2, true),
                'connote'                   => $this->faker->word(2, true),
                'origin_data'               => $this->faker->word(2, true),
                'destination_data'          => $this->faker->word(2, true)
            ]);
        $response->assertStatus(201)->assertJson(['created' => true]);
    }
}
