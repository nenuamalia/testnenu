$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    table = $("#tabel").DataTable({
        autoWidth: false,
        responsive: true,
        pagingType: "full_numbers",
        iDisplayLength: 15,
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"],
        ],
        columnDefs: [{
                targets: [0],
                data: "DT_RowIndex",
                width: "7%",
                searchable: false,
                orderable: false,
            },
            {
                targets: [1],
                visible: false,
                searchable: false,
                data: "id",
            },
            {
                targets: [2],
                data: "nama_produk",
            },
            {
                targets: [3],
                data: "image",
            },
            {
                targets: [4],
                data: "harga",
            },
            {
                targets: [5],
                data: "stock",
            },
            {
                targets: [6],
                className: "center",
                width: "15%",
                searchable: false,
                orderable: false,
                data: null,
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html(button(sData));
                },
            },
        ],
        processing: true,
        serverSide: true,
        ajax: {
            url: $("#getDataTable").val(),
            type: "POST",
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
    });

    function button(data) {
        var btn =
            '<button type="button" id="View" class="btn btn-sm btn-outline-primary" title="View">View</button>&nbsp;&nbsp;<button type="button" id="Edit" class="btn btn-sm btn-outline-success" title="Edit">Edit</button>&nbsp;&nbsp;<button type="button" id="Delete" class="btn btn-sm btn-outline-danger" title="Delete">Delete</button>';
        return btn;
    }

    $("#tabel").on("click", "#View", function () {
        var data = table.row($(this).parents("tr")).data();
        window.open($("#getIndex").val() + "/" + data.id, "_self");
    });

    $("#tabel").on("click", "#Edit", function () {
        var data = table.row($(this).parents("tr")).data();
        window.open($("#getIndex").val() + "/" + data.id + "/edit", "_self");
    });

    $("#tabel").on("click", "#Delete", function () {
        console.log($("#getDelete").val());
        var data = table.row($(this).parents("tr")).data();
        if (window.confirm("Apakah anda yakin menghapus file ini?")) {
            $.ajax({
                type: "DELETE",
                url: $("#getIndex").val() + '/' + data.id,
                success: function (data) {
                    table.ajax.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });
});
