<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Carbon\Carbon;

class Package extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'package';
    protected $primaryKey = '_id';
    public $timestamps = false;

    protected $fillable     = ['transaction_id', 'customer_name', 'customer_code', 'transaction_amount', 'transaction_state', 'transaction_code', 'transaction_order', 'location_id', 'organization_id', 'transaction_payment_type_name', 'customer_attribute', 'connote', 'origin_data', 'destination_data', 'transaction_discount', 'transaction_payment_type'];
}
