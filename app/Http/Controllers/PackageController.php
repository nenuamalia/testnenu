<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use Illuminate\Support\Facades\Validator;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * @SWG\Get(
     *   path="/api/package",
     *   summary="Get Package",
     *   operationId="package",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     * )
     *
     */

    /**
     * @SWG\Get(
     *   path="/api/package/{id}",
     *   summary="Get Package by ID",
     *   operationId="package",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string" 
     *      ),
     * )
     *
     */

    public function index($id = null)
    {
        if (!$id) {
            $data = Package::all();
        } else {
            $data = Package::findOrFail($id);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\Post(
     *   path="/api/package",
     *   summary="Create Package",
     *   operationId="createPackage",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Parameter(
     *          name="transaction_id",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_name",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_code",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_amount",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_discount",
     *          in="formData",
     *          required=false,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_payment_type",
     *          in="formData",
     *          required=false,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_state",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_code",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_order",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="location_id",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="organization_id",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_payment_type_name",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_attribute",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="connote",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="origin_data",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="destination_data",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     * )
     *
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction_id'                => 'required|unique:package',
            'customer_name'                 => 'required|string|min:2',
            'customer_code'                 => 'required|numeric',
            'transaction_amount'            => 'required|numeric',
            'transaction_discount'          => 'numeric',
            'transaction_payment_type'      => 'numeric',
            'transaction_state'             => 'required|string',
            'transaction_code'              => 'required|string|min:2',
            'transaction_order'             => 'required|numeric',
            'location_id'                   => 'required|string',
            'organization_id'               => 'required|numeric',
            'transaction_payment_type_name' => 'required|string',
            'customer_attribute'            => 'required',
            'connote'                       => 'required',
            'origin_data'                   => 'required',
            'destination_data'              => 'required',
        ]);

        $response = [
            'status'        => 'failure',
            'status_code'   => 500,
            'message'       => 'Bad Request',
            'errors'        => $validator->errors(),
        ];

        if ($validator->fails()) {
            return response()->json($response, 500, ['Content-Type' => 'application/json']);
        }

        $input = $request->all();
        Package::insert($input);

        return "Package created successfully.";

        #Receive the RAW post data.
        // $content = trim(file_get_contents("php://input"));
        #Attempt to decode the incoming RAW post data from JSON.
        // $decoded = json_decode($content, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Put(
     *   path="/api/package/{id}",
     *   summary="Update Package by ID",
     *   operationId="putPackage",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *  @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *          name="customer_name",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_code",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_amount",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_discount",
     *          in="formData",
     *          required=false,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_payment_type",
     *          in="formData",
     *          required=false,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_state",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_code",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_order",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="location_id",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="organization_id",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_payment_type_name",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_attribute",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="connote",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="origin_data",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="destination_data",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     * )
     *
     */

    /**
     * @SWG\Patch(
     *   path="/api/package/{id}",
     *   summary="Patch Package by ID",
     *   operationId="patchPackage",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *  @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *          name="customer_name",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_code",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_amount",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_discount",
     *          in="formData",
     *          required=false,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_payment_type",
     *          in="formData",
     *          required=false,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_state",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_code",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_order",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="location_id",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="organization_id",
     *          in="formData",
     *          required=true,
     *          type="number" 
     *      ),
     *  @SWG\Parameter(
     *          name="transaction_payment_type_name",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="customer_attribute",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="connote",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="origin_data",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     *  @SWG\Parameter(
     *          name="destination_data",
     *          in="formData",
     *          required=true,
     *          type="string" 
     *      ),
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'customer_name'                 => 'required|string|min:2',
            'customer_code'                 => 'required|numeric',
            'transaction_amount'            => 'required|numeric',
            'transaction_discount'          => 'numeric',
            'transaction_payment_type'      => 'numeric',
            'transaction_state'             => 'required|string',
            'transaction_code'              => 'required|string|min:2',
            'transaction_order'             => 'required|numeric',
            'location_id'                   => 'required|string',
            'organization_id'               => 'required|numeric',
            'transaction_payment_type_name' => 'required|string',
            'customer_attribute'            => 'required',
            'connote'                       => 'required',
            'origin_data'                   => 'required',
            'destination_data'              => 'required',
        ]);

        $response = [
            'status'        => 'failure',
            'status_code'   => 500,
            'message'       => 'Bad Request',
            'errors'        => $validator->errors(),
        ];

        if ($validator->fails()) {
            return response()->json($response, 500, ['Content-Type' => 'application/json']);
        }
        $data = Package::findOrFail($id);
        $data->update($request->all());

        return 'Package updated successfully.';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\Delete(
     *   path="/api/package/{id}",
     *   summary="Delete Package by ID",
     *   operationId="package",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string" 
     *      ),
     * )
     *
     */
    public function destroy($id)
    {
        $data = Package::findOrFail($id);
        $data->delete();

        return 'Package has been deleted';
    }
}
